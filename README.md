# My AppSyncLite

This was a learning experience to learn AppSync and websockets. Feel free to use the code as a learning experience or use as it. It was designed as a simple drop in tool for protoyping or use as a light weight tool instead of any heavy duty libary. For this reason my AppSyncLite was written to minimize any additional libaries.
It's primary goal is to handle the networking aspect of working with AppSync and reduce how GraphQL itself works. 


# How to use

Below is a series of simple implements and samples to show an how to use. I suggest using other samples that have more robust GraphQL samples and examples. These are the simple lazy things I made.

## Make a GraphQL AppSync Request
Make a request to the GraphQL HTTP request. This is a NUnit test sample
```
static Dictionary<string, string> _authHeaderXApiKey = new Dictionary<string, string>() { { "x-api-key", "YOUR_API_KEY" } };
const string _url = "YOUR_APPSYNC_URL_ENDPOINT";


// UNIT TEST SAMPLES
static object[] QuerySource = {
    new object[] { _url, "mutation postDataTest($id:ID!, $title:String!){ putPost(id:$id, title:$title){id, title}}", new { id = "some_id", title = "My Title" }, _authHeaderXApiKey },
    new object[] { _url, "query getDataTest($id:ID!){ singlePost(id:$id){id, title}}", new { id= "some_id" }, _authHeaderXApiKey },
};
[TestCaseSource("QuerySource")]
public async Task TestQuery(string url, string query, dynamic vars, Dictionary<string, string> header)
{
    await Query(url, query, vars, _authHeaderXApiKey);
}
public async Task Query(string url, string query, dynamic vars, Dictionary<string,string> header )
{
    var payload =   Newtonsoft.Json.JsonConvert.SerializeObject( new{
        query = query,
        variables = vars
    });

    Console.WriteLine($"Payload[{payload}]");
    var cancelToken = new CancellationToken();
    var response = await AppSyncHTTP.Request(url, payload, header, cancelToken);
    Console.WriteLine($"Response[{response}");
}

```

## Make a AppSyncLite Subscription
These steps are to make a subscription. I tried to make the process of getting a subscription with as few commands I could manage at the time. 


### Make a Subscription Object

```
const string API_URL = "https://BLARGLE_WARGLE_STUFF.REGION.amazonaws.com/graphql";
public AppSyncSubscriber appsync; // or private whatever
public void Setup()
{
	appsync = new AppSyncSubscriber(API_URL);

	// the socket subscriber is event driven. This is a sample of the events
	// that I tend to observe when developing. I use a lot of console logs
    appsync.Logger = (log) => Console.WriteLine($"Subscriber[{log}]");
    appsync.onSocketDisconnect += () => {};
    appsync.onWebSocketStateChange += (state) => {};
    appsync.onMessage += (json) => {};
    appsync.onData += (json) => { };
    appsync.onConnectionAcknowledge += (msg) => {};
    appsync.onStartAcknowledge += (msg) => {};
    appsync.onKeepAlive += (msg) => {};

	appsync.onSocketConnect += () => { 
		/* ready for subscription. */ 
		// lazy subscribe once the socket is connected
		SubscribeSample();
	};
}
```


### Connect the Socket
Once the AppSyncSubscriber is ready open a socket.
```
public void OpenSocketWithApiKey
{
	var auth = SubscriptionAuthorizationHeaderUtil
		.CreateBase64ApiKeyAuthorizationHeader(appsync.Host, API_KEY)
	appsync.Connect( auth );
}
```

### Starting a Subscription
This is a sample of of a subscription pattern. 
```
public void SubscribeSample()
{
	 var data = new
	 {
	     query = "subscription testSubscription{ onUpdateTest(id:\"AnId\"){id, value} }",
	     variables = new { }
	 };

	 var dataJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);
	 var sub = new { 
	     data = dataJson,
	     extensions = new
	     {
	         authorization = new Dictionary<string,string>()
	         {
	             { "host" , appsync.Host },
	             { "x-api-key", API_KEY },
	         }
	     }
	 };
	 
	 var json = Newtonsoft.Json.JsonConvert.SerializeObject(sub);
	 subscribeid = appsync.Subscribe(json);
}
```

### Run it

```
public void RunTheSample(){
	Setup();
	OpenSocketWithApiKey();
}
```


### Putting it all together
here is a sample of all of it together. Here is the copy and paste!!!

```
public class MyLazySubscriptionExample
{
	const string API_URL = "https://BLARGLE_WARGLE_STUFF.REGION.amazonaws.com/graphql";
	public AppSyncSubscriber appsync; // or private whatever

    static void Main(string[] args)
		RunTheSample();
	}

	public void RunTheSample(){
		Setup();
		OpenSocketWithApiKey();
	}

	public void Setup()
	{
		appsync = new AppSyncSubscriber(API_URL);

		// the socket subscriber is event driven. This is a sample of the events
		// that I tend to observe when developing. I use a lot of console logs
		appsync.Logger = (log) => Console.WriteLine($"Subscriber[{log}]");
		appsync.onSocketDisconnect += () => {};
		appsync.onWebSocketStateChange += (state) => {};
		appsync.onMessage += (json) => {};
		appsync.onData += (json) => { };
		appsync.onConnectionAcknowledge += (msg) => {};
		appsync.onStartAcknowledge += (msg) => {};
		appsync.onKeepAlive += (msg) => {};

		appsync.onSocketConnect += () => { 
			/* ready for subscription. */ 
			// lazy subscribe once the socket is connected. You should do something smarter. 
			// Such as ready the socket, and then when user interacts do something.
			SubscribeSample();
		};
	}

	public void OpenSocketWithApiKey
	{
		var auth = SubscriptionAuthorizationHeaderUtil
			.CreateBase64ApiKeyAuthorizationHeader(appsync.Host, API_KEY)
		appsync.Connect( auth );
	}


	public void SubscribeSample()
	{
		 var data = new
		 {
			 query = "subscription testSubscription{ onUpdateTest(id:\"AnId\"){id, value} }",
			 variables = new { }
		 };

		 var dataJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);
		 var sub = new { 
			 data = dataJson,
			 extensions = new
			 {
				 authorization = new Dictionary<string,string>()
				 {
					 { "host" , appsync.Host },
					 { "x-api-key", API_KEY },
				 }
			 }
		 };
	 
		 var json = Newtonsoft.Json.JsonConvert.SerializeObject(sub);
		 subscribeid = appsync.Subscribe(json);
	}
}

```




# Wrap up
This project is not a proffesional project. It's a learning experience. I hope that my experience going through this process have resulted in code that can assist in solving your own troubles with whatever your tech is using. Feel free to use whatever .dll is made. It's lightweight and can be dropped in fairly easily. I use it as a Unity compatible plugin.



