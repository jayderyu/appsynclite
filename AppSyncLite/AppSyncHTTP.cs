﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;

namespace Sre.AppSyncLite
{
    public class AppSyncHTTP
    {

        /// <summary>
        /// Sime Function call to make a url request to appsync.
        /// </summary>
        /// <param name="url">Full string url to appsync</param>
        /// <param name="payloadJson">This is a json consisting of a query string with graphql query and an object container variables used in the query</param>
        /// <param name="authHeaders">Dictionary containing authorization and cust header information(xapikey, authorization, xamzsecuritytoken)</param>
        /// <param name="cancellationToken">Cancelation token to prematurally stop the requst</param>
        /// <returns>string containing the content data from the response, on failure a status,reason,data json</returns>
        public static async Task<string> Request(
            string url, 
            string payloadJson,
            Dictionary<string, string> authHeaders,
            CancellationToken? cancellationToken = null)
        {
            var urlComponents = Utils.ParseUrlParts(url);
            var cancelToken = cancellationToken ?? new CancellationToken();

            HttpClient client = new HttpClient();
            foreach (var kvp in authHeaders)
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation(kvp.Key, kvp.Value);
            }
            
            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderKeys.Host, urlComponents.Host);

            StringContent content = new StringContent(payloadJson);

            HttpResponseMessage response = await client.PostAsync(url, content, cancelToken);

            var data = await response.Content.ReadAsStringAsync();

            /*
            if (response.IsSuccessStatusCode == false)
            {
                //Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                data = $"{{ \"status\":\"{response.StatusCode}\", \"reason\":\"{response.ReasonPhrase}\", \"data\":\"{data}\"}}";
            }
            */

            return data;
        }

    }
}
