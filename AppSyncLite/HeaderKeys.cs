﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

namespace Sre.AppSyncLite
{
    /// <summary>
    /// Contains constants for varius name, keys, properties etc. ie reduce magic strings
    /// </summary>
    public class HeaderKeys
    {
        public const string Host = "Host";
        public const string ContentType = "Content-Type";
        public const string XAmzContentSha256Header = "X-Amz-Content-SHA256";
        public const string XAmzDate = "X-Amz-Date";
        public const string XAmznTraceIdHeader = "X-Amzn-Trace-Id";
        public const string TransferEncodingHeader = "Transfer-Encoding";
        public const string XAmzSecurityToken = "X-Amz-Security-Token";
        public const string XApiKey = "x-api-key";
    }


}


