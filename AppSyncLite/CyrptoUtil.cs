﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */
using System.Security.Cryptography;
using System.Text;

namespace Sre.AppSyncLite
{

    public class CryptoUtil
    {

        public static string ToHexString(byte[] array)
        {   // https://medium.com/@chris.mckee/how-to-match-hmacsha256-between-c-python-f7b42d01cbf5
            StringBuilder hex = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }


        //http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-python
        public static string Sign(string key, string message)
        {
	        return Sign(Encoding.UTF8.GetBytes(key), message);
        }

        public static string Sign(byte[] key, string message)
        {
	        return Sign(key, Encoding.UTF8.GetBytes(message));
        }

        public static string Sign(byte[] key, byte[] message)
        {
	        HMACSHA256 hmac = new HMACSHA256(key);
	        var hash = hmac.ComputeHash(message);
	        return Encoding.ASCII.GetString(hash);
        }


        public static byte[] HmacSHA256(string data, byte[] key)
        {   // Taken from AWS Deriviate tutorial
	        string algorithm = "HmacSHA256";
	        KeyedHashAlgorithm kha = KeyedHashAlgorithm.Create(algorithm);
	        kha.Key = key;

	        return kha.ComputeHash(Encoding.UTF8.GetBytes(data));
        }

        public static byte[] GetSignatureKey(string key, string dateStamp, string regionName, string serviceName)
        {  // Taken from AWS Deriviate tutorial
            byte[] kSecret = Encoding.UTF8.GetBytes(("AWS4" + key).ToCharArray());
	        byte[] kDate = HmacSHA256(dateStamp, kSecret);
	        byte[] kRegion = HmacSHA256(regionName, kDate);
	        byte[] kService = HmacSHA256(serviceName, kRegion);
	        byte[] kSigning = HmacSHA256("aws4_request", kService);

	        return kSigning;
        }


    }


}


