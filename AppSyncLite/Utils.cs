﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Sre.AppSyncLite
{
    public class Utils
    {
        //(?:\"asin\":\")(.*?)(?:\")
        //var r = new Regex(@"(?:"asin":")(.*?)(?:")");
        private static Dictionary<string, Regex> RegexProcessor = new Dictionary<string, Regex>()
        {
            {"type",  new Regex("(?:\"type\":\")(.*?)(?:\")") },
            {"id",  new Regex("(?:\"id\":\")(.*?)(?:\")") },
            {"payload",  new Regex("(?:\"payload\":\")(.*?)(?:\")") },
        };


        /// <summary>
        /// Simple tool for pulling shallow information from a JSON string withought a serializer. Will not get a json out of the json.
        /// </summary>
        /// <param name="json"></param>
        /// <param name="field"></param>
        /// <returns>value of the json string field</returns>
        public static string GetValueFromJson(string json, string field)
        {
            var regex = RegexProcessor.ContainsKey(field)
                ? RegexProcessor[field]
                : new Regex("(?:\"" + field + "\":\")(.*?)(?:\")");
            // we could store the regex in the dict to reduce new constructions

            var match = regex.Match(json);

            if (match.Success == false) { return string.Empty; }

            var key = match.Value.Substring(0, match.Value.IndexOf(':'));
            var value = match.Value.Substring(match.Value.IndexOf(':') + 1).Replace("\"", "");

            return value;
        }


        /// <summary>
        /// Simple base 64 encoder to use for aws headers
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }




        public class UrlParseData
        {
            public string Host;
            public string Path;
            public string Service;
            public string Region;

            //public string Query;
        }


        /// <summary>
        /// Parses an amazon URL into component parts for use to compose IAM authorization
        /// </summary>
        /// <param name="url">Amazon end point url</param>
        /// <returns></returns>
        public static UrlParseData ParseUrlParts(string url)
        {
            var uri = new Uri(url);
            var sections = uri.Host.Split('.');

            return new UrlParseData()
            {
                Host = uri.Host,
                Path = uri.AbsolutePath,
                Service = sections[1].Replace("-api", ""),
                Region = sections[2]
            };
        }

    }




}
