﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */
using System.Collections.Generic;
using System.Linq;

namespace Sre.AppSyncLite
{
    public class SubscriptionAuthorizationHeaderUtil 
    {
        /// <summary>
        /// This is a convienence method for constructing a simple API key header
        /// </summary>
        /// <param name="host"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string CreateBase64ApiKeyAuthorizationHeader(string host, string key)
        {
            var headerJson = $"{{\"host\":\"{host}\", \"x-api-key\":\"{key}\"}}";
            var base64Header = Utils.Base64Encode(headerJson);

            return base64Header;
        }

        /// <summary>
        /// This is a convienece method for constructing a simple API key header
        /// </summary>
        /// <param name="host"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string CreateBase64JWTAuthorizationHeader(string host, string jwt)
        {
            var headerJson = $"{{\"host\":\"{host}\", \"Authorization\":\"{jwt}\"}}";
            var base64Header = Utils.Base64Encode(headerJson);

            return base64Header;
        }



        public static string CreateIAMAuthorizationHeaderJson(string accessKey, string secretKey, string securityToken, string graphQlUrl)
        {
            var headers = new Dictionary<string, string>() {
                {HeaderKeys.XAmzSecurityToken, securityToken }
            };

            headers = IAMV4Signer.ComputeYourWantedAuthorizationHeaders(headers, accessKey, secretKey, "POST", graphQlUrl, "{}");


            var headerJson = "{";
            headerJson += string.Join(",", //",\n"  the /n is for a more readable version
                headers.Select(kvp => $"\"{kvp.Key}\":\"{kvp.Value}\"")
                );
            headerJson += "}";

            return headerJson;
        }

        public static string CreateBase64IAMAuthorizationHeader(string accessKey, string secretKey, string securityToken, string graphQlUrl )
        {
            var headerJson = CreateIAMAuthorizationHeaderJson(accessKey, secretKey, securityToken, graphQlUrl);
            var base64Header = Utils.Base64Encode(headerJson);
            return base64Header;
        }


    }




}
