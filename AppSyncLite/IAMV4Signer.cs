﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Linq;


namespace Sre.AppSyncLite
{


    public class IAMV4Signer
    {
        private const string Algorithm = "AWS4-HMAC-SHA256";
        public const string ContentType = "application/x-amz-json-1.0";

        public static Action<string> Logger = (txt) => { Console.WriteLine(txt); };

        public static Dictionary<string, string> ComputeYourWantedAuthorizationHeaders(
             Dictionary<string, string> headers,
            string accessKey,
            string secretKey,
            string method,
            string endpoint,
            string requestParameters,
            DateTime? dateTime = null,
            bool hashCanonicalRequestData = true
            )
        {
            var components = Utils.ParseUrlParts(endpoint);

            headers = ComputeYourWantedAuthorizationHeaders(
                headers,
                accessKey,
                secretKey,
                method,
                components.Host,
                components.Path,
                components.Service,
                components.Region,
                requestParameters,
                dateTime,
                hashCanonicalRequestData);

            return headers;
        }

        public static Dictionary<string, string> ComputeYourWantedAuthorizationHeaders(
            Dictionary<string, string> headers,
            string accessKey,
            string secretKey,
            string method,
            string host,
            string canonicalUriPath, // we can get the host and uri path from the full end point
            string service,
            string region,
            string requestParametersJson, 
            DateTime? dateTime = null,
            bool hashCanonicalRequestData = true
            )
        {
            var dateTimeSign = dateTime ?? DateTime.UtcNow;// will need to do the adjust if not using in a passed in date time.
            var amzDate = dateTimeSign.ToString(DateTimeFormats.ISO8601BasicDateTimeFormat, new CultureInfo("en-US")); 
            var dateStamp = dateTimeSign.ToString(DateTimeFormats.ISO8601BasicDateFormat, new CultureInfo("en-US"));

            var outString = string.Join("\n",
	            $"Inputs",
				$"Headers: " + headers.ToString(),
	            $"Method: " + method,
	            $"region: " + region,
	            $"accessKey: " + accessKey,
	            $"secretKey: " + secretKey,
	            $"requestParametersJson: " + requestParametersJson,
	            $"canonicalUriPath: " + canonicalUriPath,
                $"dateTimeAtSigning: " + dateTimeSign.ToString("O"));
            Logger(outString);

            InitializeHeaders(headers, host, amzDate);


            // AWS4 presigned urls got S3 are expected to contain a 'UNSIGNED-PAYLOAD' magic string
            // during signing (other services use the empty-body sha)
            if (headers.ContainsKey(HeaderKeys.XAmzContentSha256Header))
                headers.Remove(HeaderKeys.XAmzContentSha256Header);

            var sortedHeaders = SortAndPruneHeaders(headers);
            
            // task 1
            var canonicalResults = CreateCanonicalRequest(
	            sortedHeaders,
	            method, 
	            requestParametersJson, 
	            canonicalUriPath,
                hashCanonicalRequestData);

            Logger(string.Join("\n",
                "Canonical Results",
	            "Signed Header: " + canonicalResults.signedHeaders,
                "PayloadHash: " + canonicalResults.payloadHash,
	            canonicalResults.canonicalRequest));

            // task 2 string to sign
            var stringToSignResults = StringToSign(canonicalResults.canonicalRequest, amzDate, dateStamp, region, service);
            Logger(string.Join("\n",
                "String To Sign",
                "credentialScope: " + stringToSignResults.credentialScope,
                "stringToSign: " + stringToSignResults.stringToSign
                ));

            // task 3 calculate signature
            var signatureResults = CalculateSignature(secretKey, stringToSignResults.stringToSign, dateStamp, region, service);

            Logger(string.Join("\n",
	            "Signature Results",
                "signingKey: " + signatureResults.signingKey,
                "signature: " + signatureResults.signature
            ));

            var authHeader = ComposeAuthorizationHeader(
                accessKey,
                stringToSignResults.credentialScope,
                canonicalResults.signedHeaders,
                signatureResults.signature);

            headers.Add("Authorization", authHeader);
            Logger("Authorization: " + authHeader);

            return headers;
        }

        private static void InitializeHeaders(Dictionary<string, string> headers, string host, string amzDateTime)
        {
            var headerCopy = new Dictionary<string, string>(headers);

            headers.Clear();

            headers.Add(HeaderKeys.ContentType, ContentType);
            headers.Add(HeaderKeys.XAmzDate, amzDateTime);
            headers.Add(HeaderKeys.Host, host);

            foreach (var kvp in headerCopy)
            {
                if (headers.ContainsKey(kvp.Key))
                {
                    headers[kvp.Key] = kvp.Value;
                }
                else
                {
                    headers.Add(kvp.Key, kvp.Value);
                }
            }
        }







        // Task 1 Create a Canonical Request
        public static (
            string canonicalRequest,
            string signedHeaders,
            string payloadHash)
            CreateCanonicalRequest( // when using tuple return the location of the method name ca fall in a funny location
            IDictionary<string, string> headers,
            string method,
            string requestParameter, 
            string canonicalUriPath, 
            bool hashCanonicalRequestData = true)
        {
            var canonicalUri = string.IsNullOrEmpty(canonicalUriPath) ? "/" : canonicalUriPath;         // will need to tackle this
            var canonicalQuerystring = "";  // will need to tackle this

            var canonicalHeaders = string.Join( string.Empty, 
                headers.Select(item => $"{ item.Key.ToLower().Trim() }:{item.Value}\n")  );

            var signedHeaders = string.Join(";", 
                headers.Keys.ToList().Select(key => key.ToLower().Trim() ));
            //https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html 
            // not sure about the \n. document says yes, but it's not their in the python tutorial
            var payloadHash = requestParameter;
            if (hashCanonicalRequestData)
            {
                payloadHash = CryptoUtil
                    .ToHexString(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(requestParameter)));
            }

            var canonicalRequest = string.Join("\n",
	            method,
	            canonicalUri,
	            canonicalQuerystring,
	            canonicalHeaders,
	            signedHeaders,
	            payloadHash);

            return (canonicalRequest, signedHeaders, payloadHash);
        }

        //https://github.com/amazon-archives/aws-sdk-unity/blob/master/Assets/AWSSDK/src/Core/Amazon.Util/HeaderKeys.cs
        private static IEnumerable<string> _headersToIgnoreWhenSigning = new HashSet<string>(StringComparer.OrdinalIgnoreCase) {
            HeaderKeys.XAmznTraceIdHeader,
            HeaderKeys.TransferEncodingHeader
        }; 
        protected static IDictionary<string, string> SortAndPruneHeaders(IEnumerable<KeyValuePair<string, string>> requestHeaders)
        { // https://github.com/aws/aws-sdk-net/blob/master/sdk/src/Core/Amazon.Runtime/Internal/Auth/AWS4Signer.cs
            var sortedHeaders = new SortedDictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (var header in requestHeaders)
            {
                if (_headersToIgnoreWhenSigning.Contains(header.Key))
                {
                    continue;
                }
                try
                {
                    sortedHeaders.Add(header.Key, header.Value);
                }
                catch(Exception e)
                {
                    Logger.Invoke($"Header {header.Key}={header.Value} duplicate found. Ignoring.\n{e}");
                }
            }

            return sortedHeaders;
        }

        // Task 2 string to sign
        public static (string stringToSign, string credentialScope) StringToSign(
            string canonicalRequest,
            string amzDate,
            string dateStamp,
            string region,
            string service)
        {
            // # ************* TASK 2: CREATE THE STRING TO SIGN*************
            // # Match the algorithm to the hashing algorithm you use, either SHA-1 or
            //# SHA-256 (recommended)
           
           var credentialScope = $"{dateStamp}/{region}/{service}/aws4_request"; // not sure about the request part.

            var encodeCanonical = Encoding.UTF8.GetBytes(canonicalRequest);
            var hash256 = SHA256.Create().ComputeHash(encodeCanonical);
            var payloadHash = CryptoUtil.ToHexString(hash256);

            var stringToSign = string.Join("\n",
                Algorithm,
                amzDate,
                credentialScope,
                payloadHash);

            return (stringToSign, credentialScope);
        }

        // calculate signature
        public static (string signature, string signingKey) CalculateSignature(string secretKey, string stringToSign, string dateStamp, string region, string service)
        {
            var signingKey = CryptoUtil.GetSignatureKey(secretKey, dateStamp, region, service);
            var signStringBytes = Encoding.UTF8.GetBytes(stringToSign);

            HMACSHA256 hmac = new HMACSHA256(signingKey);
            var hash = hmac.ComputeHash(signStringBytes);

            var signature = CryptoUtil.ToHexString(hash);
            
            return (signature, Encoding.ASCII.GetString(signingKey));
        }

        public static string ComposeAuthorizationHeader(string accessKey, string credentialScope, string signedHeaders, string signature)
        {
            return $"{Algorithm} Credential={accessKey}/{credentialScope}, SignedHeaders={signedHeaders}, Signature={signature}";
        }




        public class AWSV4SignedModel 
        { 
        }

    }


}


