﻿/* MIT License

Copyright (c) [2021] [Jason Jarvis]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// AWSAppSyncNetSimpleSubscriber, is a as small and as simple as possiple that I worked on to subscribe to AWS Appsync.
/// This projects was used as a learning experience for ClientWebSocket and connecting to appsync websocket.
/// In the process the work has gone through some updates to be as simple subscriber. The overal purpose is to get 
/// going simply by handling the handshake and then let
/// the developer handle any futher aspects by raising as much as possible to the developer.
/// In so doing aspects such as security, tacking a subscription, disconnecting subscriptions
/// are outside the controlling scope of the simple subscriber.
/// 
/// </summary>


namespace Sre.AppSyncLite
{
    /// <summary>
    /// App Sync Subscriber was to simplify my life when doing .NET subscriptions to AWS AppSync
    /// </summary>
    public class AppSyncSubscriber : IDisposable
    {
        // constants
        private const int SOCKET_STATE_MS_INTERVAL = 100;
        private const string GRAPHQL_SUB_PROTOCOL = "graphql-ws";
        private const string LOST_CONNECTION_ERROR = "{\"type\":\"_lostconnection\", \"msg\":\"abrupt disconnect\" }";

        // internal private data
        private ClientWebSocket _webSocket;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationTokenSource _cancellationTokenSourceStateChange = new CancellationTokenSource();
        private Dictionary<string, Action<string>> MessageRouter;


        // URL Urls
        public string ApiUrl { get; private set; }
        /// <summary>
        /// The AppSync GraphQL URL. Generated in the constructor from the ApiUrl. Used for socket header
        /// </summary>
        public string Host { get; private set; }
        /// <summary>
        /// The real time AppSync Url. This is generated from the host in the constructor.
        /// </summary>
        public string RealtimeUrl { get; private set; }


        // socket and exception events
        /// <summary>
        /// Web Socket as connected
        /// </summary>
        public Action onSocketConnect;
        /// <summary>
        /// Web Socket has disconnected
        /// </summary>
        public Action onSocketDisconnect;

        /// <summary>
        /// Web socket status change
        /// </summary>
        public Action<WebSocketState> onWebSocketStateChange;

        /// <summary>
        /// Well that sucks. There's a bug in the subscriber.
        /// </summary>
        public Action<Exception> onException;

        // Handshake based with AppSync
        public Action<string> onConnectionError;
        /// <summary>
        /// AppSync has finished the connection handshake and accepted the socket authorization. Your good to go have fun.
        /// </summary>
        public Action<string> onConnectionAcknowledge;
        /// <summary>
        /// Connected to AppSync Socket. Not a complete connection as of yet. Wait for Connection Acknowledged
        /// </summary>
        public Action<string> onConnection;
        /// <summary>
        /// AWS AppSync hearbeat message. Apx every 2 minutes
        /// </summary>
        public Action<string> onKeepAlive;

        // User Response from AppSync
        /// <summary>
        /// AWS AppSync Message
        /// </summary>
        public Action<string> onMessage;
        /// <summary>
        /// Uknown response from AppSync
        /// </summary>
        public Action<string> onUnknown;
        /// <summary>
        /// Subscription has started and accepted
        /// </summary>
        public Action<string> onStartAcknowledge;
        /// <summary>
        /// The fun stuff. This is the data returned from the subscription
        /// </summary>
        public Action<string> onData;
        /// <summary>
        /// Errors reported from AppSync or subscription
        /// </summary>
        public Action<string> onError;
        /// <summary>
        /// I believe this is called when AWS subscription has been correctly closed.
        /// </summary>
        public Action<string> onComplete;




        /// <summary>
        /// The total byte read size from a websocket. Change before connecting
        /// </summary>
        public int RcvByteArraySize { get; set; } = 2048;

        /// <summary>
        /// Current web socket state
        /// </summary>
        public WebSocketState SocketState => _webSocket.State;

        /// <summary>
        ///  Attach a simple logger to observe activity being processes
        /// </summary>
        public Action<string> Logger = (log) => { };


        // 0.1.0 release goal is to have an extenral callback invoker that calls the Action events on 
        // a target thread. This is personal goal as I use this with Unity and having to jump back to unity thread
        // create silly requirements. such as introducing unity async await lib and using await new WaitForEndOfFrame
        // I would rather just focus that the invoked action is just where I want it to be.
        //private Action<Action> _invoker;



        /// <summary>
        /// Constructor for the simple AppSync subscriber
        /// </summary>
        /// <param name="apiUrl">>Api URL string found in AppSync settings</param>
        /// <param name="invoker">Method that runs the message handler to the events. This allows to handle executing the
        /// events on a dedicated thread.</param>
        public AppSyncSubscriber(string apiUrl)//, Action<Action> invoker = null)
        {
            var (realTimeUrl, host) = ComposeAppSyncConnectionInformation(apiUrl);

            ApiUrl = apiUrl;
            Host = host;
            RealtimeUrl = realTimeUrl;

            // prep the web socket
            _webSocket = new ClientWebSocket();
            _webSocket.Options.AddSubProtocol(GRAPHQL_SUB_PROTOCOL);

            //_invoker = invoker != null ? invoker : InternalMessageInvoke;

            InitRouter();

            // run the state thread
            RunWebSocketState();
        }
        

        /// <summary>
        /// Eventually plan to make routers public. Part of the 1.0.0 release
        /// </summary>
        private void InitRouter()
        {
            MessageRouter = new Dictionary<string, Action<string>>()
            {
                {"connection_error",(msg)=>{ 
                    _cancellationTokenSource.Cancel();
                    CloseSocket("AWS Connection Error");
                    InvokeCallback(onMessage, msg);
                    InvokeCallback(onConnectionError, msg); }  
                },
                {   "connection_ack",  (msg)=>{
                        //ConnectionTimeoutMs = long.Parse( Utils.GetValueFromJson(msg, "connectionTimeoutMs" ) );
                        InvokeCallback(onMessage, msg); 
                        InvokeCallback(onConnectionAcknowledge, msg); 
                    } 
                },
                {"ka",              (msg)=>{ InvokeCallback(onMessage, msg); InvokeCallback(onKeepAlive, msg);} },

                {"complete",        (msg)=>{ InvokeCallback(onMessage, msg); InvokeCallback(onComplete, msg); } },
                {"error",           (msg)=>{ InvokeCallback(onMessage, msg); InvokeCallback(onError, msg); } },
                {"data",            (msg)=>{ InvokeCallback(onMessage, msg); InvokeCallback(onData, msg); } },
                {"start_ack",       (msg)=>{ InvokeCallback(onMessage, msg); InvokeCallback(onStartAcknowledge, msg); }  },
                {"_forward",        (msg)=>{ InvokeCallback(onMessage, msg); InvokeCallback(onUnknown, msg); } },
                {"_lostconnection", (msg)=>{
                    _cancellationTokenSource.Cancel();
                    CloseSocket("Lost Connetion");
                    InvokeCallback(onMessage, msg);
                    InvokeCallback(onConnectionError, msg);

                }
                },
            };
        }

        private void InvokeCallback<T>(Action<T> actions, T msg)
        {
            if (actions == null) return;
            foreach (var action in actions.GetInvocationList() )
            {
                try
                {
                    action.DynamicInvoke(msg);
                }
                catch(Exception e)
                {
                    InvokeCallback<Exception>(onException, e);
                }
            }
        }
        private void InvokeCallback(Action actions)
        {
            if (actions == null) return;
            foreach (var action in actions.GetInvocationList())
            {
                try
                {
                    action.DynamicInvoke();
                }
                catch (Exception e)
                {
                    InvokeCallback<Exception>(onException, e);
                }
            }
        }


        /// <summary>
        /// Handles the connection and handshake process for connecting to AppSync
        /// </summary>
        /// <param name="authorizationJsonHeaderBase64">This is base 64 json for the authorization connection. Check out  https://docs.aws.amazon.com/appsync/latest/devguide/real-time-websocket-client.html for composing the correct header</param>
        /// <param name = "handshakeCompleteCallback" >Callback which will be invoked when the connection flow is complete </param>
        /// <returns>Task for sync/awaits</returns>
        public void Connect( 
            string authorizationJsonHeaderBase64, 
            Action handshakeCompleteCallback = null, 
            Action<Exception> connectionExceptionCallback = null)
        {
            Logger?.Invoke($"[{Thread.CurrentThread.ManagedThreadId}]AppSyncSubscriber.ConnectAuthHeader: Running on thread[{Thread.CurrentThread.ManagedThreadId}]");

            var url = $"{RealtimeUrl}?header={authorizationJsonHeaderBase64}&payload=e30=";
            Task.Run(async () =>
            {
                await ConnectAsync(url, handshakeCompleteCallback, connectionExceptionCallback);
            });
        }

        private async Task ConnectAsync(
            string url, 
            Action handshakeCompleteCallback = null, 
            Action<Exception> connectionExceptionCallback = null)
        {
            _cancellationTokenSource = new CancellationTokenSource();

            _webSocket = new ClientWebSocket();
            _webSocket.Options.AddSubProtocol(GRAPHQL_SUB_PROTOCOL);
            try
            {
                await _webSocket.ConnectAsync(new Uri(url), _cancellationTokenSource.Token).ConfigureAwait(true);
            }
            catch (Exception e)
            {
                InvokeCallback<Exception>(connectionExceptionCallback, e);
                return;
            }

            RunReciever();

            InvokeCallback(onSocketConnect);

            var payload = CreateAppSyncMessage("connection_init", null, null);

            await Send(payload).ConfigureAwait(true);

            InvokeCallback(handshakeCompleteCallback);
        }


        /// <summary>
        /// Sends a fully composed appsync json string
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public async Task Send(string json)
        {
            var bytes = Encoding.Default.GetBytes(json);
            var message = new ArraySegment<byte>(bytes);

            // should we do a logger here?

            try
            {
                await _webSocket.SendAsync(message, WebSocketMessageType.Text, true, _cancellationTokenSource.Token);
            }
            catch (Exception e)
            {
                InvokeCallback<Exception>(onException, e);
            }
        }



        /// <summary>
        /// Starts the subscription process
        /// </summary>
        /// <param name="graphQlJson">This is the payload of the appsync message</param>
        /// <returns>The GUID to properly ID and or end a subscription</returns>
        public string Subscribe(string graphQlJson)
        {
            var guid = System.Guid.NewGuid().ToString("B").ToUpper().Replace("{","").Replace("}","");
            var msg = CreateAppSyncMessage("start", guid, graphQlJson);

            // Logger?

            Task.Run(async () =>
            {
                await Send(msg);
            }, 
            CancellationToken.None)
                .ConfigureAwait(true);

            return guid;
        }

        /// <summary>
        /// Ubsubsribe from appsync
        /// </summary>
        /// <param name="subscribeId">The is passed in from a sucessful subscription.</param>
        public void UnSubscribe(string subscribeId)
        {
            var msg = CreateAppSyncMessage("stop", subscribeId, null);

            Task.Run(async () =>
            {
                await Send(msg);
            },
            CancellationToken.None)
                .ConfigureAwait(true);

        }

        

        static StringBuilder jsonBuilder = new StringBuilder();
        /// <summary>
        /// Lazy method used to construct appsync json handshake messages without a serializer
        /// </summary>
        /// <param name="type">the type of AppSync message</param>
        /// <param name="id">optional subscription id for the composition. </param>
        /// <param name="payloadJson"></param>
        /// <returns></returns>
        private static string CreateAppSyncMessage( string type, string id, string payloadJson)
        {
            jsonBuilder.Clear();

            jsonBuilder.Append("{");
            jsonBuilder.Append("\"type\":\"" + type + "\"");

            if (string.IsNullOrEmpty(id) == false)
            {
                jsonBuilder.Append(",\"id\":\"" + id + "\"");
            }

            if (string.IsNullOrEmpty(payloadJson) == false)
            {
                jsonBuilder.Append(",\"payload\": " + payloadJson );
            }

            jsonBuilder.Append("}");

            return jsonBuilder.ToString();
        }

        /// <summary>
        /// This observes the incoming websocket information and forwards to handler
        /// </summary>
        private void RunReciever()
        {
            byte[] rcvByteArr = new byte[RcvByteArraySize];
            var buffer = new ArraySegment<byte>(rcvByteArr);

            var task = Task.Run(
                async () =>
                {
                    //?? yes it's canceled but not working
                    while
                    (_webSocket != null && _webSocket.State == WebSocketState.Open)
                    {
                        // may want a log, or event to let a known when the socket has gone from recieved and back into waiting mode.

                        try
                        {
                            var result = await _webSocket.ReceiveAsync(buffer, _cancellationTokenSource.Token);
                        }
                        catch (NullReferenceException ex)
                        {
                            var nullException = new NullReferenceException("Subscriber.RunReciever: _webSocket is null. Is there an attempt to connect during a disconnect", ex);
                            onException?.Invoke(nullException);
                        }
                        catch (Exception e)
                        {
                            onException?.Invoke(e);
                        }

                        var msg = Encoding.UTF8.GetString(buffer.Array);

                        Logger?.Invoke($"Subscriber.Rcv: {msg}");

                        if (buffer.Array[0] == 0 && SocketState != WebSocketState.Aborted)
                        {
                            msg = LOST_CONNECTION_ERROR;
                            Logger.Invoke("Socket State " + SocketState.ToString());
                            if (_cancellationTokenSource != null)
                            {
                                Logger.Invoke("_cancellationTokenSource.IsCancellationRequested " + _cancellationTokenSource.IsCancellationRequested);
                            }

                        }

                        HandleMessage(msg);

                        Array.Clear(rcvByteArr, 0, msg.Length);
                        await Task.Delay(1);
                    }

                    // something happened here
                    try
                    {
                        Logger.Invoke($"Subscriber.RunReciever: Sync Thread task ended SocketState[{SocketState}] tokenCanceled[{_cancellationTokenSource.IsCancellationRequested}]");
                    }catch (Exception e)
                    {
                        Logger.Invoke($"{e}");
                    }
                },
                _cancellationTokenSource.Token)
                .ConfigureAwait(true);

        }

        private void InternalMessageInvoke(Action action)
        {
            action();
        }

        private void HandleMessage(string msg)
        {
            var type = Utils.GetValueFromJson(msg, "type");

            var route = MessageRouter.ContainsKey(type) ? type : "_forward";

            // some how reduce garbage with the invoker
            //   _invoker.Invoke( ()=> { MessageRouter[route](msg);  } );
            MessageRouter[route](msg);
        }


        private void RunWebSocketState()
        {
            WebSocketState state = WebSocketState.None;


            Task.Run(async () =>
            {
                while (_cancellationTokenSourceStateChange.IsCancellationRequested == false)
                {
                    if (state != _webSocket.State)
                    {
                        state = _webSocket.State;
                        InvokeCallback<WebSocketState>(onWebSocketStateChange, state);
                    }

                    await Task.Delay(SOCKET_STATE_MS_INTERVAL);
                }
            },
            _cancellationTokenSourceStateChange.Token)
                .ConfigureAwait(true);
        }

        /// <summary>
        /// Close the websocket
        /// </summary>
        /// <param name="reason"></param>
        public async void CloseSocket(string reason)
        {
            Logger.Invoke($"CloseSocket: Closing socket[{_webSocket.State}] for {reason}");
            if( _webSocket.State == WebSocketState.Aborted )
            {
                Logger.Invoke($"CloseSocket: Socket state is {WebSocketState.Aborted} and is not allowed");
                return;
            }


            try
            {
                // I don't think we need to "close" if we cancel the token
                await _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, reason, CancellationToken.None);
                //await _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, reason, _cancellationTokenSource);
            }
            catch (Exception e)
            {
                InvokeCallback(onException, e);
            }

            try
            {
                if (_cancellationTokenSource != null && _cancellationTokenSource.IsCancellationRequested == false)
                {
                    _cancellationTokenSource.Cancel();
                }
            }
            catch (Exception e)
            {
                InvokeCallback(onException, e);
            }


            await Task.Delay(100);
            Logger.Invoke($"CloseSocket: Socket state is {_webSocket.State}");
        }


        /// <summary>
        /// Disconnect from AppSync, but this does not close the socket.
        /// </summary>
        public async void Disconnect()
        {
            try
            {
                if( _cancellationTokenSource != null && _cancellationTokenSource.IsCancellationRequested == false )
                    _cancellationTokenSource.Cancel();
            }
            catch (Exception e)
            {
                InvokeCallback(onException, e);
            }

            try
            {
                await _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Requested Close", CancellationToken.None);
            }
            catch (Exception e)
            {
                InvokeCallback(onException, e);
            }

            try
            {
                if(_cancellationTokenSource != null)
                    _cancellationTokenSource.Dispose();
            }
            catch (Exception e)
            {
                InvokeCallback(onException, e);
            }

            InvokeCallback(onSocketDisconnect);
        }


        /// <summary>
        /// Used to Dispose the Subscriber
        /// </summary>
        public void Dispose()
        {

            try
            {
                if (_cancellationTokenSource != null && _cancellationTokenSource.IsCancellationRequested == false)
                {
                    _cancellationTokenSource.Cancel();
                }

                if (_cancellationTokenSourceStateChange != null)
                {
                    _cancellationTokenSourceStateChange.Cancel();
                }
            }
            catch (Exception e)
            {
                Logger.Invoke($"");
                InvokeCallback(onException, e);
            }


        }








        /// <summary>
        /// Convenient method to handle converting the appsync url to other connection infomration components.
        /// </summary>
        /// <param name="apiUrl">The AppSync URL found on the appsync settings.</param>
        /// <returns>(realtime url websocket, host)</returns>
        public (string, string) ComposeAppSyncConnectionInformation(string apiUrl)
        {
            string host = apiUrl;
            string realTimeUrl = apiUrl;

            host = host.Replace("https://", "").Replace("/graphql", "");
            realTimeUrl = realTimeUrl.Replace("https", "wss").Replace("appsync-api", "appsync-realtime-api");

            return (realTimeUrl, host);
        }




   

    }
}
